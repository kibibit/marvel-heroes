import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { PowersModule } from './powers/powers.module'
import { PrismaModule } from './prisma/prisma.module';
import { CitiesModule } from './cities/cities.module';

@Module({
  imports: [PowersModule, PrismaModule, CitiesModule],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
