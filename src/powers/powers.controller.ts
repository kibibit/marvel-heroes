import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common'
import { PowerDto } from './powers.dto'
import { PowersService } from './powers.service'
import { Power as PowerModel } from '@prisma/client'

@Controller('powers')
export class PowersController {
  constructor(private powerService: PowersService) {}

  @Get()
  findAll(): Promise<PowerModel[]> {
    return this.powerService.findAll()
  }

  @Get(':powerId')
  findPower(@Param('powerId') powerId: string): Promise<PowerModel> {
    return this.powerService.findPower({ id: Number(powerId) })
  }

  @Post()
  createPower(@Body() newPower: PowerDto): Promise<PowerModel> {
    const { name, description } = newPower
    return this.powerService.createPower({ name, description })
  }

  @Delete(':powerId')
  deletePower(@Param('powerId') powerId: string): Promise<PowerModel> {
    return this.powerService.deletePower({ id: Number(powerId) })
  }

  @Put(':powerId')
  updatePower(@Param('powerId') powerId: string, @Body() newPowerData: PowerDto): Promise<PowerModel> {
    const { name, description } = newPowerData
    return this.powerService.updatePower({ id: Number(powerId) }, { name, description })
  }
}
