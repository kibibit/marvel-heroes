import { Injectable } from '@nestjs/common'
import { PrismaService } from 'src/prisma/prisma.service'
import { Power, Prisma } from '@prisma/client'

@Injectable()
export class PowersService {
  constructor(private prisma: PrismaService) {}

  findAll(): Promise<Power[]> {
    return this.prisma.power.findMany()
  }

  findPower(powerId: Prisma.PowerWhereUniqueInput): Promise<Power | null> {
    return this.prisma.power.findUnique({ where: powerId })
  }

  createPower(newPower: Prisma.PowerCreateInput): Promise<Power> {
    return this.prisma.power.create({ data: newPower })
  }

  deletePower(powerId: Prisma.PowerWhereUniqueInput): Promise<Power> {
    return this.prisma.power.delete({ where: powerId })
  }

  updatePower(powerId: Prisma.PowerWhereUniqueInput, newPowerData: Prisma.PowerUpdateInput): Promise<Power> {
    return this.prisma.power.update({ where: powerId, data: newPowerData })
  }
}
