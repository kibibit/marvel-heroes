export class PowerDto {
  readonly name: string
  readonly description: string
}
