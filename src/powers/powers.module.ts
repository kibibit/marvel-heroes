import { Module } from '@nestjs/common'
import { PowersService } from './powers.service'
import { PowersController } from './powers.controller'

@Module({
  providers: [PowersService],
  controllers: [PowersController]
})
export class PowersModule {}
