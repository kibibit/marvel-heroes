import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common'
import { CitiesService } from './cities.service'
import { CityDto } from './dto/city.dto'
import { City as CityModel } from '@prisma/client'

@Controller('cities')
export class CitiesController {
  constructor(private readonly citiesService: CitiesService) {}

  @Post()
  create(@Body() createCityDto: CityDto): Promise<CityModel> {
    return this.citiesService.create(createCityDto)
  }

  @Get()
  findAll(): Promise<CityModel[]> {
    return this.citiesService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<CityModel> {
    return this.citiesService.findOne({ id: Number(id) })
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() newCityData: CityDto): Promise<CityModel> {
    const { name } = newCityData
    return this.citiesService.update({ id: Number(id) }, { name })
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<CityModel> {
    return this.citiesService.remove({ id: Number(id) })
  }
}
