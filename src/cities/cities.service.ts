import { Injectable } from '@nestjs/common'
import { PrismaService } from 'src/prisma/prisma.service'
import { City, Prisma } from '@prisma/client'

@Injectable()
export class CitiesService {
  constructor(private prisma: PrismaService) {}

  create(newCity: Prisma.CityCreateInput): Promise<City> {
    return this.prisma.city.create({ data: newCity })
  }

  findAll(): Promise<City[]> {
    return this.prisma.city.findMany()
  }

  findOne(id: Prisma.CityWhereUniqueInput): Promise<City | null> {
    return this.prisma.city.findUnique({ where: id })
  }

  update(cityId: Prisma.CityWhereUniqueInput, newCityData: Prisma.CityUpdateInput): Promise<City> {
    return this.prisma.city.update({ where: cityId, data: newCityData })
  }

  remove(cityId: Prisma.CityWhereUniqueInput): Promise<City> {
    return this.prisma.power.delete({ where: cityId })
  }
}
