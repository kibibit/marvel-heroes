-- CreateEnum
CREATE TYPE "Condition" AS ENUM ('FREE', 'JAILED', 'UNKNOWN');

-- CreateTable
CREATE TABLE "Power" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT,

    CONSTRAINT "Power_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "City" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "City_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Group" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Group_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Vehicle" (
    "id" SERIAL NOT NULL,
    "description" TEXT NOT NULL,
    "type" TEXT NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "Vehicle_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Character" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "isHero" BOOLEAN NOT NULL,
    "cityId" INTEGER NOT NULL,
    "condition" "Condition" NOT NULL,
    "groupId" INTEGER,

    CONSTRAINT "Character_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "KnownPowers" (
    "characterId" INTEGER NOT NULL,
    "powerId" INTEGER NOT NULL,

    CONSTRAINT "KnownPowers_pkey" PRIMARY KEY ("characterId","powerId")
);

-- CreateIndex
CREATE UNIQUE INDEX "Vehicle_userId_key" ON "Vehicle"("userId");

-- AddForeignKey
ALTER TABLE "Vehicle" ADD CONSTRAINT "Vehicle_userId_fkey" FOREIGN KEY ("userId") REFERENCES "Character"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Character" ADD CONSTRAINT "Character_cityId_fkey" FOREIGN KEY ("cityId") REFERENCES "City"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Character" ADD CONSTRAINT "Character_groupId_fkey" FOREIGN KEY ("groupId") REFERENCES "Group"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KnownPowers" ADD CONSTRAINT "KnownPowers_powerId_fkey" FOREIGN KEY ("powerId") REFERENCES "Power"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "KnownPowers" ADD CONSTRAINT "KnownPowers_characterId_fkey" FOREIGN KEY ("characterId") REFERENCES "Character"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

CREATE VIEW "CharacterViews" AS
    SELECT ch.id,
        ch.name,
        ch."isHero",
        ci.name AS city,
        ch.condition,
        gr.name AS "group",
        ARRAY( SELECT po.name
            FROM "Power" po
                JOIN "KnownPowers" kp ON po.id = kp."powerId"
            WHERE kp."characterId" = ch.id) AS powers,
        vh.type AS "vehicleType"
    FROM "Character" ch
        LEFT JOIN "City" ci ON ch."cityId" = ci.id
        LEFT JOIN "Group" gr ON ch."groupId" = gr.id
        LEFT JOIN "Vehicle" vh ON ch.id = vh."userId";
