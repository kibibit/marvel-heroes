import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

async function main() {
  console.log('creating cities...')
  const newYorkCity = await prisma.city.create({ data: { name: 'New York' } })
  const town = await prisma.city.create({ data: { name: 'Town in germany' } })

  console.log('adding powers to db...')
  const strength = await prisma.power.create({ data: { name: 'SuperHuman Strength', description: '' } })
  const telepathy = await prisma.power.create({ data: { name: 'Telepathy', description: '' } })
  const sniper = await prisma.power.create({ data: { name: 'Sniper', description: 'Expert Shooter' } })
  const acrobat = await prisma.power.create({
    data: { name: 'Acrobat', description: 'Expert Acrobat and Martial Artist' }
  })
  const eco = await prisma.power.create({ data: { name: 'Ecolocalization', description: 'Sonic radar that can see' } })

  console.log('creating characters...')
  const redSkull = await prisma.character.create({
    data: {
      name: 'Red Skull',
      isHero: false,
      city: { connect: { id: town.id } },
      condition: 'JAILED',
      group: {
        create: {
          name: 'HYDRA'
        }
      }
    }
  })
  const CaptainAmerica = await prisma.character.create({
    data: {
      name: 'Captain America',
      isHero: true,
      city: { connect: { id: newYorkCity.id } },
      condition: 'FREE',
      group: {
        create: {
          name: 'Avengers'
        }
      },
      vehicle: {
        create: {
          description: 'Military Royal Enfield',
          type: 'MotorCycle'
        }
      }
    }
  })
  const Daredevil = await prisma.character.create({
    data: {
      name: 'Daredevil',
      isHero: true,
      city: { connect: { id: newYorkCity.id } },
      condition: 'FREE',
      group: {
        create: {
          name: 'The Defenders'
        }
      }
    }
  })

  console.log('creating powers...')
  await prisma.knownPowers.createMany({
    data: [
      { characterId: redSkull.id, powerId: strength.id },
      { characterId: redSkull.id, powerId: telepathy.id },
      { characterId: redSkull.id, powerId: sniper.id },
      { characterId: CaptainAmerica.id, powerId: strength.id },
      { characterId: CaptainAmerica.id, powerId: acrobat.id },
      { characterId: Daredevil.id, powerId: sniper.id },
      { characterId: Daredevil.id, powerId: acrobat.id },
      { characterId: Daredevil.id, powerId: eco.id }
    ]
  })
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
